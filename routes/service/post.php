<?php
//Llamada al archivo de conexión y el controlador POST
require_once "models/connection.php";
require_once("controllers/post.controller.php");

if(isset($_POST)){

    //Obtenemos el nombre de la tabla por la ruta
    $table = explode("?", $routesArray[2])[0];

    //Registro de usuario
    if (isset($_GET["register"]) && $_GET["register"] == true) {
        $sufix = $_GET["sufix"] ?? "usuario";
        $response = new PostController();
        $response -> postRegister($table,$_POST,$sufix);
    }
    //Login de usuario
    elseif (isset($_GET["login"]) && $_GET["login"] == true) {
        $sufix = $_GET["sufix"] ?? "usuario";
        $response = new PostController();
        $response -> postLogin($table,$_POST,$sufix);
    }
    //Post para recuperar contraseña por codigo
    elseif (isset($_GET["recuperar"]) && $_GET["recuperar"] == true){
        //Generar código y expiración
        $email = $_POST["correo_usuario"];
        $codigoRecuperación = Connection::codigoRecuperacion();
        $response = new PostController();
        $response -> postEmail($email,$codigoRecuperación);
        $_POST["codigo_usuario"] = $codigoRecuperación;
        $time = time();
        $_POST["codigo_exp_usuario"] = $time + (60*5);
        $response -> postData($table,$_POST);
    }else{
        //Validamos el token para poder hacer esta operación en la base de datos
        if(isset($_GET["token"])){
            $tableToken = $_GET["table"];
            $sufix = $_GET["sufix"];
            $valido = Connection::tokenValido($_GET["token"],$tableToken,$sufix);
            //Ok si el token es válido
            if($valido == "Ok"){
                $response = new PostController();
                $response -> postData($table,$_POST);
            }
            //Expirado si el token caduco
            if($valido == "Expirado"){
                $json = array(
                    'status' => 303,
                    'res' => 'Error El token ha expirado'
                );
                echo json_encode($json,http_response_code($json["status"]));
                return;
            }
            //No autorizado si el token es incorrecto
            if($valido == "No autorizado"){
                $json = array(
                    'status' => 400,
                    'res' => 'Error El usuario no es autorizado'
                );
                echo json_encode($json,http_response_code($json["status"]));
                return;
            }
        //Si no se envia un token
        }else{
            $json = array(
                'status' => 400,
                'res' => 'Autorización requerida'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
    }
}

?>