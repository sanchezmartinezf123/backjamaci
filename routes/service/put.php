<?php
//Llamada a los archivos de conexión y al controlador PUT
require_once "models/connection.php";
require_once("controllers/put.controller.php");

if (isset($_GET["id"]) && isset($_GET["nameId"])) {
    //Obtenemos la tabla desde la ruta
    $table = explode("?", $routesArray[2])[0];
    $data = array();
    parse_str(file_get_contents("php://input"),$data);
    
    //Verificamos que el token sea válido para poder alterar la tabla 
    if(isset($_GET["token"])){
        $tableToken = $_GET["table"];
        $sufix = $_GET["sufix"];
        $valido = Connection::tokenValido($_GET["token"],$tableToken,$sufix);
        //Ok si el token es válido
        if($valido == "Ok"){
            $response = new PutController();
            $response -> putData($table,$data,$_GET["id"],$_GET["nameId"]);
        }
        //Expirado si el token caduco
        if($valido == "Expirado"){
            $json = array(
                'status' => 303,
                'res' => 'Error El token ha expirado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
        //No autorizado si el token no es el correcto
        if($valido == "No autorizado"){
            $json = array(
                'status' => 400,
                'res' => 'Error El usuario no es autorizado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
    //Aquí no válidamos el token porque es el proceso de recuperar contraseña 
    }elseif(isset($_GET["updPass"]) && $_GET["updPass"] == true){
        $crypt = crypt($data["password_usuario"], '$2a$07$usesomesillystringforsalt$');
        $data["password_usuario"] = $crypt;
        $response = new PutController();
        $response -> putData($table,$data,$_GET["id"],$_GET["nameId"]);
    }
    //Si el token no es enviado
    else{
        $json = array(
            'status' => 400,
            'res' => 'Autorización requerida'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }
}

?>