<?php
//Llamadas a la conexión y controlador GET
require_once "models/connection.php";
require_once("controllers/get.controller.php");

//Se obtiene el nombre de la tabla desde la ruta
$table = explode("?", $routesArray[2])[0];

$select = $_GET["select"] ?? "*";
$orderBy = $_GET["orderBy"] ?? null;
$orderMode = $_GET["orderMode"] ?? null;
$startAt = $_GET["startAt"] ?? null;
$endAt = $_GET["endAt"] ?? null;
$codigo = $_GET["codigo"] ?? null;
$groupBy = $_GET["groupBy"] ?? null;
$differTo = $_GET["differTo"] ?? null;

$response = new GetController();

//GET para obtener el código al momento de recuperar la contraseña 
if(isset($_GET["codigo"])){
    $valido = Connection::codigoValido($codigo,$table);
    if($valido === 'Ok'){
        $json = array(
            'status' => 200,
            'res' => 'Ok'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }elseif($valido === 'Expirado'){
        $json = array(
            'status' => 200,
            'res' => 'Expirado'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }elseif($valido === 'El código no es igual'){
        $json = array(
            'status' => 200,
            'res' => 'Error'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }
}

//GET para datos con filtro
elseif (isset($_GET["linkTo"]) && isset($_GET["equalTo"]) && !isset($_GET["rel"]) && !isset($_GET["type"])) {
    $response -> getDataFilter(
        $table,
        $select,
        $_GET["linkTo"],
        $_GET["equalTo"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
    );
}

//GET para tablas relacionadas 
elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations" && !isset($_GET["linkTo"]) && !isset($_GET["equalTo"])) {
    $response -> getRelData(
        $_GET["rel"],
        $_GET["type"],
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
    );
}

//GET para tablas relacionadas con datos filtrados 
elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations" && isset($_GET["linkTo"]) && isset($_GET["equalTo"])) {
    $response -> getRelDataFilter(
        $_GET["rel"],
        $_GET["type"],
        $select,
        $_GET["linkTo"],
        $_GET["equalTo"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
    );
}

//GET para hacer obtener datos con SEARCH
elseif (isset($_GET["linkTo"]) && isset($_GET["search"]) && !isset($_GET["rel"]) && !isset($_GET["type"])) {
    $response -> getDataSearch(
        $table,
        $select,
        $_GET["linkTo"],
        $_GET["search"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}

//GET para tablas relacionadas con SEARCH 
elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations" && isset($_GET["linkTo"]) && isset($_GET["search"])) {
    $response -> getRelDataSearch(
        $_GET["rel"],
        $_GET["type"],
        $select,
        $_GET["linkTo"],
        $_GET["search"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}

//GET para obtener datos sin filtro ni relaciones 
else{
    $response -> getData(
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
    );
}

?>