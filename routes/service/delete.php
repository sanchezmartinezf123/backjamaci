<?php
//Llamada al controlador DELETE
require_once("controllers/delete.controller.php");

if (isset($_GET["id"]) && isset($_GET["nameId"])) {
    
    //Se obtiene el nombre de la tabla desde la ruta
    $table = explode("?", $routesArray[2])[0];
    
    //Se valida que el token sea el correcto y que no haya expirado 
    if(isset($_GET["token"])){
        $tableToken = $_GET["table"];
        $sufix = $_GET["sufix"];
        $valido = Connection::tokenValido($_GET["token"],$tableToken,$sufix);
        //Devuelve un Ok si el token es válido
        if($valido == "Ok"){
            $response = new DeleteController();
            $response -> deleteData($table,$_GET["id"],$_GET["nameId"]);
        }
        //Devuelve Expirado si el token ya vencio
        if($valido == "Expirado"){
            $json = array(
                'status' => 303,
                'res' => 'Error El token ha expirado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
        //No autorizado si el token no es el correcto
        if($valido == "No autorizado"){
            $json = array(
                'status' => 400,
                'res' => 'Error El usuario no es autorizado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
    //Si no envia ningún token 
    }else{
        $json = array(
            'status' => 400,
            'res' => 'Autorización requerida'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }
}

?>