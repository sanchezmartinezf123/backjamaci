<?php
//Llamadas a los modelos POST, GET, PUT y archivo connection de la base de datos 
require_once("models/post.Model.php");
require_once("models/get.Model.php");
require_once("models/put.Model.php");
require_once("models/connection.php");

//Para encriptar contraseñas 
require_once ("vendor/autoload.php");
require 'vendor/autoload.php';

//Uso de Firebase y PHPMailer para usar el JWT y usar el envio de correos electrónicos
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Controlador del método POST
class PostController{

    //Función para enviar datos a la tabla 
    static public function postData($table,$data){
        $response = PostModel::postData($table,$data);
        $return = new PostController();
        $return -> fncResponse($response,null);
    }

    //Función para el registro de un usuario
    static public function postRegister($table,$data,$sufix){
        if(isset($data["password_".$sufix]) && $data["password_".$sufix] != null){
            //Encriptación de la contraseña
            $crypt = crypt($data["password_".$sufix], '$2a$07$usesomesillystringforsalt$');
            $data["password_".$sufix] = $crypt;
            $response = PostModel::postData($table,$data);
            $return = new PostController();
            $return -> fncResponse($response,null);
        }else{
            $response = PostModel::postData($table,$data);
            if (isset($response["comment"]) && $response["comment"] == "Dato creado") {
                $response = GetModel::getDataFilter($table,"*","email_".$sufix,$data["email_".$sufix],null,null,null,null,null,null);
                if (!empty($response)) {
                    $token = Connection::jwt($response[0]->{"id_".$sufix}, $response[0]->{"email_".$sufix});
                    $key = 'key';
                    $jwt = JWT::encode($token,$key,"HS256");
                    //Actualizar la DB
                    $data = array(
                        "token_".$sufix => $jwt,
                        "token_exp_".$sufix => $token["exp"]
                    );
                    //Actualización del token
                    $update = PutModel::putData($table,$data,$response[0]->{"id_".$sufix},"id_".$sufix);
                    if (isset($update["comment"]) && $update["comment"] == "Dato editado") {
                        $response[0]->{"token_".$sufix} = $jwt;
                        $response[0]->{"token_exp_".$sufix} = $token["exp"];
                        $return = new PostController();
                        $return -> fncResponse($response,null);
                    }
                }
            }
        }
    }

    //Función para hacer login
    static public function postLogin($table,$data,$sufix){
        $response = GetModel::getDataFilter($table,"*","email_".$sufix,$data["email_".$sufix],null,null,null,null,null,null);
        if (!empty($response)) {
            if($response[0]->{"password_".$sufix} != null){
                $crypt = crypt($data["password_".$sufix], '$2a$07$usesomesillystringforsalt$');
                if ($response[0]->{"password_".$sufix} == $crypt) {
                    $token = Connection::jwt($response[0]->{"id_".$sufix}, $response[0]->{"email_".$sufix});
                    $key = 'key';
                    $jwt = JWT::encode($token,$key,"HS256");
                    //Actualizar la DB
                    $data = array(
                        "token_".$sufix => $jwt,
                        "token_exp_".$sufix => $token["exp"]
                    );
                    //Se actualiza los datos con el token nuevo
                    $update = PutModel::putData($table,$data,$response[0]->{"id_".$sufix},"id_".$sufix);
                    if (isset($update["comment"]) && $update["comment"] == "Dato editado") {
                        $response[0]->{"token_".$sufix} = $jwt;
                        $response[0]->{"token_exp_".$sufix} = $token["exp"];
                        $return = new PostController();
                        $return -> fncResponse($response,null);
                    }
                    return;
                }else{
                    $response = null;
                    $return = new PostController();
                    $return -> fncResponse($response,"Password malo");
                }
            //Login cuando se hace con una red social 
            }else{
                $token = Connection::jwt($response[0]->{"id_".$sufix}, $response[0]->{"email_".$sufix});
                    $key = 'key';
                    $jwt = JWT::encode($token,$key,"HS256");
                    //Actualizar la DB
                    $data = array(
                        "token_".$sufix => $jwt,
                        "token_exp_".$sufix => $token["exp"]
                    );
                    $update = PutModel::putData($table,$data,$response[0]->{"id_".$sufix},"id_".$sufix);
                    if (isset($update["comment"]) && $update["comment"] == "Dato editado") {
                        $response[0]->{"token_".$sufix} = $jwt;
                        $response[0]->{"token_exp_".$sufix} = $token["exp"];
                        $return = new PostController();
                        $return -> fncResponse($response,null);
                    }
            }
        }else{
            $response = null;
            $return = new PostController();
            $return -> fncResponse($response,"Email malo");
        }
    }

    //Función para enviar un email 
    static public function postEmail($email,$codigo){
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();                                     
            $mail->Host       = 'smtp.gmail.com';                     
            $mail->SMTPAuth   = true;                                  
            $mail->Username   = 'franm8057@gmail.com';                     
            $mail->Password   = 'lsolykxcevhjngkp';                               
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            
            $mail->Port       = 465;
            $mail->setFrom('franm8057@gmail.com', 'Jamaci');
            $mail->addAddress($email, 'Usuario');
            $mail->isHTML(true);                                  
            $mail->Subject = 'Codigo de recuperacion';
            $mail->Body    = 'Código disponible por 5 minutos: ' .$codigo;   
            $mail->send();
        } catch (Exception $e) {
        }
    } 

    //Función de respuesta 
    public function fncResponse($response,$error){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                'res' => $response
            );
        }else {
            if ($error != null) {
                $json = array(
                    'status' => 200,
                    'res' => $error
                ); 
            }else{
                $json = array(
                    'status' => 404,
                    'res' => 'Not found',
                    "method" => "POST"
                ); 
            }
        }

        echo json_encode($json,http_response_code($json["status"]));
    }

}

?>