<?php
//Llamada al modelo PUT
require_once("models/put.Model.php");

//Controlador del método PUT
class PutController{

    //Función para actualizar datos 
    static public function putData($table,$data,$id,$nameId){
        $response = PutModel::putData($table,$data,$id,$nameId);
        $return = new PutController();
        $return -> fncResponse($response);
    }

    //Función de respuesta 
    public function fncResponse($response){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                'res' => $response
            );
        }else {
            $json = array(
                'status' => 404,
                'res' => 'Not found',
                "method" => "PUT"
            ); 
        }
        echo json_encode($json,http_response_code($json["status"]));
    }
    
}

?>