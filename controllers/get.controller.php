<?php
//Llamada el modelo GET
require_once("models/get.Model.php");

//Controlador del método GET 
class GetController{    
    
    //Función para obtener un dato sin filtros
    static public function getData(
        
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
        ){
        $response = GetModel::getData(
            $table,
            $select,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt,
            $groupBy
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    //Función para obtener datos con filtros 
    static public function getDataFilter(
        $table,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
        ){
        $response = GetModel::getDataFilter(
            $table,
            $select,
            $linkTo,
            $equalTo,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt,
            $groupBy,
            $differTo
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    //Función para obtener datos en relaciones 
    static public function getRelData(
        $rel,
        $type,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
        ){
        $response = GetModel::getRelData(
            $rel,
            $type,
            $select,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt,
            $groupBy
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    //Función para obtener datos filtrados en relaciones 
    static public function getRelDataFilter(
        $rel,
        $type,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
        ){
        $response = GetModel::getRelDataFilter(
            $rel,
            $type,
            $select,
            $linkTo,
            $equalTo,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt,
            $groupBy,
            $differTo
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    //Función de que devuelve la respuesta 
    public function fncResponse($response){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                "total" => count($response),
                'res' => $response
            );
        }else {
            $json = array(
                'status' => 200,
                'res' => 'Not found'
            ); 
        }

        echo json_encode($json,http_response_code($json["status"]));
    }

    //Función para obtener datos con SEARCH
    static public function getDataSearch(
        $table,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getDataSearch(
            $table,
            $select,
            $linkTo,
            $search,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    //Función para obtener datos con SEARCH usando relaciones 
    static public function getRelDataSearch(
        $rel,
        $type,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getRelDataSearch(
            $rel,
            $type,
            $select,
            $linkTo,
            $search,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

}


?>