<?php
//CORS para poder ser accedidos en Angular 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header('content-type: application/json; charset-utf-8');

//Key que se obtiene al crear el servicio en Firebase 
$severKey="AAAAaBW8bTk:APA91bG6M3YBWeJdeo9fNXto5JMs2RP07fx3FeE-2mLMSYO6L1UOAjUT1kWmwpBvFBbmPRAVxgB4yDcfrQrrrc6o3v5wLFsXD-gFiC30vC-6fskbP-3TSMG1LfYku7S-IhXc4NF2yovl";

//url para enviar notificaciones desde una API
$url='https://fcm.googleapis.com/fcm/send';

//Headers para la API
$header=array(
    'Authorization: key='.$severKey,
    'Content-Type: application/json'
);

//Variable que contiene los datos del mensaje 
$field = [
    'title'=>$_GET["title"],
    'body'=>$_GET["body"],
    'icon'=>'https://jamaci.com/assets/logo-Jamaci/jamaci.png'
];

//Body de la notificación 
$apiBody = [
    'notification' =>$field,
    'data' => $field,
    'to'=>$_GET['to']
];

//Se usa curl para poder ejecutar el archivo
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiBody));

$result=curl_exec($ch);
curl_close($ch);
return $result; 

?>