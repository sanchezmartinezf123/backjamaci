# TABLAS
El nombre de la tabla se coloca en la ruta por ejemplo:
http://localhost/backjamaci/nombre_tabla

# VENDOR
Esta carpeta contiene los archivos de composer, firebase y php mailer
Composer = Para instalar dependencias al proyecto
Firebase =  Para usar JWT
PHPMailer = Para usar el envió de correos

# NOTIFICACIONES
Esta carpeta contiene el archivo necesario para enviar notificaciones PUSH, esto lo hace por medio de un token que se genera en Firebase,
si es que lo hace local y para solo prueba, pero si se necesita dinámico tendra que hacer otras cosas del lado del cliente
en el proyecto JamaciFrontEnd se incluye dicho proceso. 

# USO DEL PROYECTO
Para poder usar el proyecto guardelo en la carpeta htdocs de XAMPP y probarlo directamente desde el navegador o en POSTMAN
por medio de APIS. 

