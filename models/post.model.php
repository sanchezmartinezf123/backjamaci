<?php
//Llamada al archivo de conexión 
require_once "connection.php";

//Modelo POST
class PostModel{

    //Función para enviar datos 
    static public function postData($table,$data){
        $columns = "";
        $params = "";
        foreach ($data as $key => $value) {
            $columns .= $key.",";
            $params .= ":".$key.",";
        }
        $columns = substr($columns,0,-1);
        $params = substr($params,0,-1);
        $sql = "INSERT INTO $table ($columns) VALUES ($params)";
        $stmt = Connection::connect()->prepare($sql);
        foreach ($data as $key => $value) {
            $stmt->bindParam(":".$key,$data[$key],PDO::PARAM_STR);
        }
        if ($stmt -> execute()){
            $response = array(
                "comment" => "Dato creado"
            );
            return $response;
        }else{
            return Connection::connect()->erroInfo();
        }
    }

}
?>