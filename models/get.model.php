<?php
//Llamada al archivo de conexión 
require_once "connection.php";

//Modelo GET
class GetModel{
    
    //Función GET cuando solo se obtienen los datos sin filtros 
    static public function getData(
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
        ){

        if (empty(Connection::getColumnsData($table))) {
            return null;
        }

        $sql = "SELECT $select FROM $table";

        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table LIMIT $startAt, $endAt";
        }

        if($orderBy == null && $orderMode == null && $startAt == null && $endAt == null && $groupBy != null){
            $sql = "SELECT $select FROM $table GROUP BY $groupBy";
        }

        if($orderBy != null && $orderMode != null && $startAt != null && $endAt != null && $groupBy != null){
            $sql = "SELECT $select FROM $table GROUP BY $groupBy ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }


        $stmt = Connection::connect()->prepare($sql);
        $stmt -> execute();

        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    //Función GET cuando se obtienen datos con filtros 
    static public function getDataFilter(
        $table,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
        ){
            if (empty(Connection::getColumnsData($table))) {
                return null;
            }
        $linkToArray = explode(",",$linkTo);
        $equalToArray = explode(",",$equalTo);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText";
       
        
        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText LIMIT $startAt, $endAt";
        }

        if($orderBy == null && $orderMode == null && $startAt == null && $endAt == null && $groupBy != null){
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null && $groupBy != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null && $groupBy != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy ORDER BY $orderBy $orderMode";
        }

        //If especial solo para buscar un valor diferente de, por ejemplo rol!=0, es decir usar el != en la cláusula WHERE pero solo una vez
        if ($orderBy != null && $orderMode != null && $differTo != null && $startAt == null && $endAt == null && $groupBy != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText AND $differTo GROUP BY $groupBy ORDER BY $orderBy $orderMode";
        }

        $stmt = Connection::connect()->prepare($sql);
        
        foreach ($linkToArray as $key => $value) {
            $stmt -> bindParam(':'.$value,$equalToArray[$key],PDO::PARAM_STR);
        }

        $stmt -> execute();
        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    //Función GET cuando se usan relaciones entre tablas 
    static public function getRelData(

        $rel,
        $type,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if (empty(Connection::getColumnsData($value))) {
                    return null;
                }
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText LIMIT $startAt, $endAt";
            }

            if($orderBy == null && $orderMode == null && $startAt == null && $endAt == null && $groupBy != null){
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText GROUP BY $groupBy";
            }


            $stmt = Connection::connect()->prepare($sql);
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

    //Función GET cuando se usan relaciones entre tablas y además se filtran datos 
    static public function getRelDataFilter(
        $rel,
        $type,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $groupBy,
        $differTo
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);
        $linkToArray = explode(",",$linkTo);
        $equalToArray = explode(",",$equalTo);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                // if (empty(Connection::getColumnsData($value))) {
                //     return null;
                // }
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null && $groupBy != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy LIMIT $startAt, $endAt ";
            }

            if ($orderBy == null && $orderMode == null && $startAt == null && $endAt == null && $groupBy != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy";
            }

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null && $groupBy != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP BY $groupBy ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $differTo != null && $startAt == null && $endAt == null && $groupBy != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText WHERE $linkToArray[0] = :$linkToArray[0] $linkToText AND estado != $differTo GROUP BY $groupBy ORDER BY $orderBy $orderMode";
            }



            $stmt = Connection::connect()->prepare($sql);
            foreach ($linkToArray as $key => $value) {
                $stmt -> bindParam(':'.$value,$equalToArray[$key],PDO::PARAM_STR);
            }
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

    //Función GET cuando se filtra un dato con SEARCH
    static public function getDataSearch(
        $table,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        $linkToArray = explode(",",$linkTo);
        $searchArray = explode("_",$search);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                if($key > 0){
                    
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText";

        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText LIMIT $startAt, $endAt";
        }


        $stmt = Connection::connect()->prepare($sql);
        foreach ($linkToArray as $key => $value) {
            if ($key > 0) {
                $stmt -> bindParam(':'.$value,$searchArray[$key],PDO::PARAM_STR);
            }
        }
        $stmt -> execute();

        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    //Función GET cuando se usan relaciones y se filtran datos con SEARCH
    static public function getRelDataSearch(
        $rel,
        $type,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);
        $linkToArray = explode(",",$linkTo);
        $searchArray = explode("_",$search);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if (empty(Connection::getColumnsData($value))) {
                    return null;
                }
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText LIMIT $startAt, $endAt";
            }


            $stmt = Connection::connect()->prepare($sql);
            foreach ($linkToArray as $key => $value) {
                if ($key > 0) {
                    $stmt -> bindParam(':'.$value,$searchArray[$key],PDO::PARAM_STR);
                }
            }
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

}

?>