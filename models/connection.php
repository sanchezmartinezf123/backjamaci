<?php
//Llamada al modelo GET
require_once "get.model.php";

class Connection{
    
    //Datos de conexión a la base de datos 
    static public function infoDatabase(){
        $infoDB = array(
            "database" => "jamaci",
            "user" => "root",
            "pass" => ""
        );
        return $infoDB;
    }

    //Conexión a la base de datos 
    static public function connect(){
        try {
            $link = new PDO(
                "mysql:host=localhost;dbname=".Connection::infoDatabase()["database"],
                Connection::infoDatabase()["user"],
                Connection::infoDatabase()["pass"]
            );
            $link->exec("set names utf8");
        } catch (PDOException $e) {
            die("Error: ".$e.getMessage());
        }
        return $link;
    }

    //Función general para obtener la tabla de la base de datos y los campos que cada una de ellas tiene 
    static public function getColumnsData($table){
        $database = Connection::infoDatabase()["database"];
        return Connection::connect()
        ->query("SELECT COLUMN_NAME AS item FROM information_schema.columns WHERE table_schema = '$database' AND table_name = '$table' ")
        ->fetchAll(PDO::FETCH_OBJ);
    }

    //Función para generar Token de Auth
    static public function jwt($id,$email){
        $time = time();
        $token = array(
            "iat" => $time,
            "exp" => $time * (60*60*24),
            "data" => [
                "id" => $id,
                "email" => $email
            ]
        );
        return $token;
    }

    //Función para generar código de recuperación
    static public function codigoRecuperacion(){
        $key="";
        $tamaño=4;
        $pattern="123456789abcdefghijklmnopqrstuvwxyz";
        $max=strlen($pattern)-1;
        for($i=0;$i<$tamaño;$i++){
            $key .= substr($pattern,mt_rand(0,$max),1);
        }
        return $key;
    }

    //Función para validar que el código de recuperación sea válido 
    static public function codigoValido($codigo,$table){
        $user = GetModel::getDataFilter(
            $table,
            "codigo_exp_usuario",
            "codigo_usuario",
            $codigo,
            null,
            null,
            null,
            null,
            null,
            null
        );
        if (!empty($user)) {
            //Validamos que el código no haya expirado
            $time = time();
            if ($time < $user[0]->{"codigo_exp_usuario"}) {
                return "Ok";
            }else{
                return "Expirado";
            }
        }else{
            return "El código no es igual";
        }
    }

    //Función para validar el Token de seguridad
    static public function tokenValido($token,$table,$sufix){
        //Traemos el token de acuerdo al usuarios
        $user = GetModel::getDataFilter(
            $table,
            "token_exp_".$sufix,
            "token_".$sufix,
            $token,
            null,
            null,
            null,
            null,
            null,
            null
        );
        if (!empty($user)) {
            //Validamos que el token no haya expirado
            $time = time();
            if ($time < $user[0]->{"token_exp_".$sufix}) {
                return "Ok";
            }else{
                return "Expirado";
            }
        }else{
            return "No autorizado";
        }
    }

     //ApiKey de la API 
    static public function apikey(){
        return "qwertyuiop";
    }

}

?>