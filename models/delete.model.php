<?php
//Llamada al archivo de conexión 
require_once "connection.php";

//Modelo DELETE
class DeleteModel{
    
    //Función para eliminar datos 
    static public function deleteData($table,$id,$nameId){
        $sql = "DELETE FROM $table WHERE $nameId =:$nameId";
        $stmt = Connection::connect()->prepare($sql);
        $stmt->bindParam(":".$nameId,$id,PDO::PARAM_STR);
        if ($stmt -> execute()){
            $response = array(
                "comment" => "Dato eliminado"
            );
            return $response;
        }else{
            return Connection::connect()->erroInfo();
        }
    }
    
}

?>