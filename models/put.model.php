<?php
//Llamada al archivo de conexión 
require_once "connection.php";

//Modelo PUT
class PutModel{

    //Función para actualizar datos 
    static public function putData($table,$data,$id,$nameId){
        $set = "";
        foreach ($data as $key => $value) {
            $set .= $key." = :".$key.",";
        }
        $set = substr($set,0,-1);
        $sql = "UPDATE $table SET $set  WHERE $nameId = :$nameId";
        $stmt = Connection::connect()->prepare($sql);
        foreach ($data as $key => $value) {
            $stmt->bindParam(":".$key,$data[$key],PDO::PARAM_STR);
        }
        $stmt->bindParam(":".$nameId,$id,PDO::PARAM_STR);
        if ($stmt -> execute()){
            $response = array(
                "comment" => "Dato editado"
            );
            return $response;
        }else{
            return Connection::connect()->erroInfo();
        }
    }

}

?>